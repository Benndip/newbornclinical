import { StyleSheet } from 'react-native';

import theme from '../../theme';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: 140,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10,
        padding: 0
    },
    card: {
        width: '80%',
        height: '80%',
        borderRadius: 10
    },
    timeView: {
        borderRadius: 10,
        width: '100%',
        paddingVertical: 5,
        paddingLeft: 5
    },
    iconAndGender: {
        padding: 5,
        alignItems: 'center',
    },
    detailsCard: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5,
        borderWidth: 0,
        justifyContent: 'space-between'
    }
});

export default styles;