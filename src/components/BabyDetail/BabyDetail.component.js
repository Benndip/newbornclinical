import React from 'react'
import { View, Text } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { useSelector } from 'react-redux';

import styles from './BabyDetail.style';
import { Card } from '../../components'

const BabyDetail = ({ time, owner, location, gender, backgroundColor, lightColor }) => {

    const i18n = useSelector(state => state.LanguageReducer.i18n)

    return (
        <View style={[styles.container, { backgroundColor: backgroundColor.dark }]}>
            <Card style={styles.card}>
                <View style={[styles.timeView, { backgroundColor: backgroundColor.light }]}>
                    <Text>{time + ' ' + i18n.t('phrases.minutes_from_birth')}</Text>
                </View>
                <View style={styles.detailsCard}>
                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5 }}>
                            <Text style={{ fontWeight: 'bold' }}>{i18n.t('words.baby')} </Text>
                            <Text>{i18n.t('words.of')} </Text>
                            <Text style={{ fontWeight: 'bold' }}>{owner}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text>{i18n.t('words.location')}: </Text>
                            <Text style={{ fontWeight: 'bold' }}>{location}</Text>
                        </View>
                    </View>
                    <View style={styles.iconAndGender}>
                        <FontAwesome5
                            name={gender === 'male' ? 'male' : 'female'}
                            size={25}
                            color='#576574'
                        />
                        <Text style={{ fontSize: 12 }}>{gender === 'male' ? i18n.t('words.male') : i18n.t('words.female')}</Text>
                    </View>
                    <EvilIcons
                        name='chevron-right'
                        size={25}
                    />
                </View>
            </Card>
        </View>
    )
}

export default BabyDetail
