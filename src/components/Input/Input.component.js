import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import styles from './Input.style';
import theme from '../../theme'

const Input = ({ label, iconName, usingMateriaCommIcon, makeSecured, placeholder, errorMessage, errorState, style, props }) => {

    const [passwordNotShown, setpasswordNotShown] = useState(true);
    label
    const togglePasswordVissibility = () => {
        setpasswordNotShown(!passwordNotShown)
    }

    return (
        <>
            <View style={styles.viewForLabel}>
                <Text style={styles.labelTxt}>{label}</Text>
            </View>
            <View style={[styles.container, { ...style }]}>
                <TextInput
                    style={styles.input}
                    {...props}
                    secureTextEntry={makeSecured && (passwordNotShown ? true : false)}
                    placeholderTextColor='#a29bfe'
                    autoCapitalize='none'
                    autoCorrect={false}
                    color='#000'
                    placeholder={placeholder}
                />
                {
                    makeSecured && (
                        <TouchableOpacity
                            style={styles.secureIconView}
                            onPress={togglePasswordVissibility}
                        >
                            {
                                passwordNotShown
                                    ?
                                    <MaterialCommunityIcons name='eye-off-outline' color={theme.PRIMARY_COLOR} size={26} />
                                    :
                                    <MaterialCommunityIcons name='eye-outline' color={theme.PRIMARY_COLOR} size={26} />
                            }

                        </TouchableOpacity>
                    )
                }
            </View>
            {errorState && <Text style={styles.errorMessage}>{errorMessage}</Text>}
        </>
    )
}

export default Input;
