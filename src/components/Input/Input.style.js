import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen')

import theme from '../../theme'

const styles = StyleSheet.create({
    container: {
        borderColor: theme.PRIMARY_COLOR,
        width: '80%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 10,
        borderRadius: 45,
        alignSelf: 'center',
        borderWidth: 1,
    },
    input: {
        width: '83%',
        height: '100%',
        paddingHorizontal: 10,
        backgroundColor: 'transparent',
        color: 'red'
    },
    secureIconView: {
        position: 'absolute',
        right: 10
    },
    errorMessage: {
        color: '#ee5253',
        alignSelf: 'flex-start',
        marginLeft: '3%',
        fontSize: 11
    },
    viewForLabel: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        width: 90,
        height: 40,
        top: 30,
        left: 35,
        zIndex: 9999,
        // borderWidth: 1,
        paddingHorizontal: 10
    },
    labelTxt: {
        color: theme.PRIMARY_COLOR,
        fontSize: 14
    }
});

export default styles;