import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen')

import theme from '../../theme'

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 20,
        paddingHorizontal: '2%',
    },
    circle: {
        width: 40,
        height: 40,
        borderRadius: 20
    }
});

export default styles;