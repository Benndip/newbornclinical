import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { useSelector } from 'react-redux';

import styles from './AssesCard.style';
import theme from '../../theme';
import COLORS from '../../../assets/data/colors';

const AssesCard = ({ owner, location, eatTime, status }) => {

    const [toggleCheckBox, setToggleCheckBox] = React.useState(false)

    const i18n = useSelector(state => state.LanguageReducer.i18n)

    return (
        <TouchableOpacity style={styles.container}>
            <View style={[styles.circle, { backgroundColor: status === 'Danger' ? '#eb2f06' : COLORS[Math.floor(Math.random() * 4)].dark }]} />
            <View style={styles.ownerAndLocatinView}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontWeight: 'bold' }}>{i18n.t('words.baby')} </Text>
                    <Text>{i18n.t('words.of')} </Text>
                    <Text style={{ fontWeight: 'bold' }}>{owner}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 12, color: '#a4b0be' }}>{i18n.t('words.location')}: </Text>
                    <Text style={{ fontSize: 12, color: '#a4b0be' }}>{location}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 12, color: '#a4b0be' }}>{i18n.t('words.EAT')} </Text>
                    <Text style={{ fontSize: 12, color: '#a4b0be' }}>{eatTime}</Text>
                </View>
            </View>
            <View style={{ marginTop: 24 }}>
                <Text style={{ fontSize: 12, color: '#747d8c' }}>{i18n.t('words.status')}</Text>
                <Text style={{ fontSize: 12, color: status === 'Danger' ? '#eb2f06' : theme.PRIMARY_COLOR, fontWeight: 'bold' }}>
                    {status === 'Danger' ? i18n.t('words.danger') : i18n.t('words.normal')}
                </Text>
            </View>
            <CheckBox
                disabled={false}
                value={toggleCheckBox}
                onValueChange={(newValue) => setToggleCheckBox(newValue)}
                tintColors={{ true: theme.PRIMARY_COLOR, false: '#ccc' }}
            />
        </TouchableOpacity>
    )
}

export default AssesCard
