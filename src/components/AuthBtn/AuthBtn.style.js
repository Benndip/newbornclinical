import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen')

import theme from '../../theme'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        borderColor: theme.PRIMARY_COLOR,
        borderWidth: 1,
        width: '50%',
        height: 50,
        alignItems: 'center',
        paddingHorizontal: 10,
        borderRadius: 45,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    authBtnTxt: {
        color: theme.PRIMARY_COLOR,
        fontSize: 16
    }
});

export default styles;