import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

import styles from './AuthBtn.style'

const AuthBtn = ({ name, style, onPress }) => {
    return (
        <TouchableOpacity
            style={[styles.container, { ...style }]}
            onPress={onPress}
        >
            <Text style={styles.authBtnTxt}>{name}</Text>
        </TouchableOpacity>
    )
}

export default AuthBtn
