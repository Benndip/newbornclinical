import React from 'react';
import { View, Text } from 'react-native';

import styles from './Header.style';

const Header = ({ children, style }) => {
    return (
        <View style={[styles.container, { ...style }]}>
            {children}
        </View>
    )
}

export default Header
