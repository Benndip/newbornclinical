import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen')

import theme from '../../theme'

const styles = StyleSheet.create({
   container:{
       width: '100%',
       backgroundColor: theme.PRIMARY_COLOR,
       borderBottomStartRadius: 25,
       borderBottomEndRadius: 25
   }
});

export default styles;