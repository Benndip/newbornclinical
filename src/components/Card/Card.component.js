import React from 'react';
import { View, Text, Pressable } from 'react-native';

import styles from './Card.style';

const Card = ({ children, style, onPress }) => {
    return (
        <Pressable
            style={[styles.container, { ...style }]}
            onPress={onPress}
        >
            {children}
        </Pressable>
    )
}

export default Card
