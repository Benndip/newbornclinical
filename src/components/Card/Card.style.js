import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen')

import theme from '../../theme'

const styles = StyleSheet.create({
    container: {
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 1.5,
        shadowOpacity: 0.1,
        elevation: 4,
        borderWidth:0.6,
        borderColor: '#ddd',
        backgroundColor: '#ffffff',
    }
});

export default styles;