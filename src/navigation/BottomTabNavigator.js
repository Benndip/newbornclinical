import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { useSelector } from 'react-redux';

import theme from '../theme'
import TopTabNavigator from './TopTabNavigator'
import { Home, Babies, Profile } from '../screens'

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {

    const i18n = useSelector(state => state.LanguageReducer.i18n)

    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: theme.PRIMARY_COLOR,
                inactiveTintColor: 'gray',
            }}
        >
            <Tab.Screen name="Home" component={Home}
                options={{
                    tabBarLabel: i18n.t('words.home'),
                    tabBarIcon: ({ color, size, focused }) => (
                        <Ionicons name={focused ? "home" : "home-outline"} color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Babies" component={Babies}
                options={{
                    tabBarLabel: i18n.t('phrases.list_of_babies'),
                    tabBarIcon: ({ color, size, focused }) => (
                        <MaterialCommunityIcons name={focused ? "baby-face" : "baby-face-outline"} color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Notifications" component={TopTabNavigator}
                options={{
                    tabBarBadge: 3,
                    tabBarBadgeStyle: { fontSize: 12 },
                    tabBarLabel: i18n.t('words.notifications'),
                    tabBarIcon: ({ color, size, focused }) => (
                        <Ionicons name={focused ? "notifications" : "notifications-outline"} color={color} size={size} />
                    ),
                }}
            />
            <Tab.Screen name="Profile" component={Profile}
                options={{
                    tabBarLabel: i18n.t('words.profile'),
                    tabBarIcon: ({ color, size, focused }) => (
                        <FontAwesome name={focused ? "user" : "user-o"} color={color} size={size} />
                    ),
                }}
            />
        </Tab.Navigator>
    )
}

export default BottomTabNavigator
