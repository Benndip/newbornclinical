import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Initial, Splash, Login } from '../screens';
import DrawerNavigation from './DrawerNavigation';

const stack = createStackNavigator();

const MainNavigator = () => {
    return (
        <stack.Navigator
            initialRouteName='Splash'
            detachInactiveScreens
            screenOptions={{ headerShown: false }}
        >
            <stack.Screen name="Splash" component={Splash} />
            <stack.Screen name="Initial" component={Initial} />
            <stack.Screen name="Login" component={Login} />
            <stack.Screen name="DrawerNavigation" component={DrawerNavigation} />
        </stack.Navigator>
    );
};

export default MainNavigator
