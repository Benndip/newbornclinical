import React from 'react'
import { View, Text } from 'react-native'
import { createDrawerNavigator } from '@react-navigation/drawer';

import BottomTabNavigator from './BottomTabNavigator';
import { DrawerContent } from '../screens'

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
    return (
        <Drawer.Navigator
            initialRouteName="BottomTabNavigator"
            drawerContent={props => <DrawerContent {...props} />}
        >
            <Drawer.Screen name="BottomTabNavigator" component={BottomTabNavigator} />
        </Drawer.Navigator>
    )
}

export default DrawerNavigation
