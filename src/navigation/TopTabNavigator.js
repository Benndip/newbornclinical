import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useSelector } from 'react-redux';

import { Assesment, Alerts } from '../screens'

const Tab = createMaterialTopTabNavigator();


const TopTabNavigator = () => {

    const i18n = useSelector(state => state.LanguageReducer.i18n)

    return (
        <Tab.Navigator>
            <Tab.Screen name="Assesment" component={Assesment}
                options={{
                    tabBarLabel: i18n.t('phrases.risk_assessments')
                }}
            />
            <Tab.Screen name="Alerts" component={Alerts}
                options={{
                    tabBarLabel: i18n.t('phrases.monitoring_alerts')
                }}
            />
        </Tab.Navigator>
    )
}

export default TopTabNavigator
