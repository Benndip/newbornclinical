import { combineReducers } from 'redux';

import LanguageReducer from './LanguageReducer'

const rootReducer = combineReducers({
    LanguageReducer,
})

export default rootReducer
