import * as actionTypes from '../actions/actionTypes';
import i18n from 'i18n-js';

import en from '../../translations/en'
import fr from '../../translations/fr'

const initState = {
    i18n: i18n
}

const LanguageReducer = (state = initState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_LANGUAGE:
            const locale = action.payload
            // storage.save({
            //     key: 'LOCALE',
            //     data: locale,
            //     expires: 1000 * 3600 * 24 * 365,
            // })
            i18n.locale = locale;
            i18n.fallbacks = true;
            i18n.translations = { en, fr };

            //update redux state
            let newI18n = { ...i18n }
            return {
                ...state,
                i18n: newI18n
            }

        default:
            return state;
    }
}

export default LanguageReducer