import * as actionTypes from './actionTypes';

export const changeLanguage = (locale) => {
    return {
        type: actionTypes.CHANGE_LANGUAGE,
        payload: locale
    }
}