import Splash from './Splash/Splash.screen';
import Initial from './Initial/Initial.screen';
import Login from './Login/Login.screen';
import Home from './Home/Home.screen'
import Babies from './Babies/Babies.screen';
import Profile from './Profile/Profile.screen';
import Assesment from './Assesment/Assesment.screen';
import Alerts from './Alerts/Alerts.screen';
import DrawerContent from './DrawerContent/DrawerContent.screen';

export {
    Splash,
    Initial,
    Login,
    Home,
    Babies,
    Profile,
    Assesment,
    Alerts,
    DrawerContent
}