import React, { useState, useEffect } from 'react'
import { useRef } from 'react'
import { View, Text, StatusBar, Animated } from 'react-native'
import * as RNLocalize from "react-native-localize";
import { useSelector, useDispatch } from 'react-redux';


import styles from './Splash.style'
import * as actions from '../../redux/actions/actions'
import theme from '../../theme'

const Splash = ({ navigation }) => {

    const opacity = useRef(new Animated.Value(0)).current

    //from redux store
    const i18n = useSelector(state => state.LanguageReducer.i18n)
    const dispatch = useDispatch()

    const animateOpacity = () => {
        Animated.timing(opacity, {
            toValue: 1,
            duration: 1600,
            useNativeDriver: true
        }).start()
    }

    const userLanguage = () => {
        let currentDeviceLocale = RNLocalize.getLocales()[0].languageCode;
        currentDeviceLocale ? dispatch(actions.changeLanguage(currentDeviceLocale)) : dispatch(actions.changeLanguage("en"))
    }

    useEffect(() => {
        userLanguage()
        animateOpacity()
        const timeout = setTimeout(() => {
            navigation.navigate("Initial")
        }, 2000);
        return () => {
            clearTimeout(timeout)
        }
    }, [])

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.PRIMARY_COLOR} />
            <Animated.Image
                source={require('../../../assets/images/ecbc_logo.png')}
                style={[styles.logo, { opacity }]}
            />
            <Animated.Text style={[styles.txt, { opacity }]}>{i18n.t('phrases.essential_care_for_every_baby')}</Animated.Text>
        </View>
    )
}

export default Splash
