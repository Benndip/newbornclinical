import React, { useState } from 'react'
import { View, Text, Platform, UIManager, LayoutAnimation, TouchableOpacity } from 'react-native'
import { Drawer } from 'react-native-paper'
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { useSelector, useDispatch } from 'react-redux';


import styles from './DrawerContent.style';
import * as actions from '../../redux/actions/actions';
import theme from '../../theme';

if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}

const DrawerContent = () => {

    const i18n = useSelector(state => state.LanguageReducer.i18n)
    const dispatch = useDispatch()

    const [selectLanguage, setSelectLanguage] = useState(false)
    let currentLocale = i18n.locale

    return (
        <View style={styles.container}>
            <DrawerContentScrollView>
                <Drawer.Section>
                    <DrawerItem
                        icon={({ color, size }) => (
                            <FontAwesome
                                name='home'
                                size={23}
                                color={theme.PRIMARY_COLOR}
                            />
                        )}
                        label={i18n.t('words.home')}
                        onPress={() => { }}
                    />
                    <View>
                        <DrawerItem
                            icon={({ color, size }) => (
                                <FontAwesome
                                    name='language'
                                    size={23}
                                    color={theme.PRIMARY_COLOR}
                                />
                            )}
                            label={i18n.t('words.language')}
                            onPress={() => {
                                LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                                setSelectLanguage(prev => !prev)
                            }}
                        />
                        {
                            selectLanguage ?
                                <EvilIcons
                                    name='chevron-down'
                                    size={33}
                                    color={theme.PRIMARY_COLOR}
                                    style={{ position: 'absolute', right: 10, top: 16 }}
                                /> :
                                <EvilIcons
                                    name='chevron-right'
                                    size={33}
                                    color={theme.PRIMARY_COLOR}
                                    style={{ position: 'absolute', right: 10, top: 16 }}
                                />
                        }
                        {
                            selectLanguage &&
                            <>
                                <TouchableOpacity
                                    style={{ justifyContent: 'space-between', paddingHorizontal: 69, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}
                                    onPress={() => {
                                        dispatch(actions.changeLanguage('en'))
                                    }}
                                >
                                    <Text>{i18n.t('words.english')}</Text>
                                    <View style={{ backgroundColor: currentLocale === 'en' ? theme.PRIMARY_COLOR : '#fff', width: 15, height: 15, borderRadius: 70.5, borderWidth: 0.3 }} />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{ justifyContent: 'space-between', paddingHorizontal: 69, marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}
                                    onPress={() => {
                                        dispatch(actions.changeLanguage('fr'))
                                    }}
                                >
                                    <Text>{i18n.t('words.french')}</Text>
                                    <View style={{ backgroundColor: currentLocale === 'fr' ? theme.PRIMARY_COLOR : '#fff', width: 15, height: 15, borderRadius: 70.5, borderWidth: 0.3 }} />
                                </TouchableOpacity>
                            </>

                        }
                    </View>
                </Drawer.Section>
            </DrawerContentScrollView>
        </View>
    )
}

export default DrawerContent
