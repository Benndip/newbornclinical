import React from 'react'
import { View, Text, StatusBar, ScrollView, Image } from 'react-native'
import { useSelector } from 'react-redux';

import styles from './Login.style';
import { AuthBtn, Input } from '../../components'
import theme from '../../theme'

const Login = ({ navigation }) => {

    //from redux store
    const i18n = useSelector(state => state.LanguageReducer.i18n)

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.PRIMARY_COLOR} />
            <View style={styles.firstView}>
                <Image
                    source={require('../../../assets/images/ecbc_logo.png')}
                    style={styles.logo}
                />
                <Text style={styles.essentialTxt}>ECEB</Text>
            </View>
            <ScrollView keyboardShouldPersistTaps='always' style={{ with: '100%' }}>
                <Input
                    style={styles.input}
                    label={i18n.t('words.email')}
                    placeholder='...@example.com'
                />
                <Input
                    makeSecured
                    style={styles.input}
                    label={i18n.t('words.password')}
                    placeholder='*******'
                />
                <AuthBtn
                    style={styles.loginBtn}
                    name={i18n.t('words.login')}
                    onPress={() => navigation.navigate('DrawerNavigation')}
                />
            </ScrollView>
        </View>
    )
}

export default Login
