import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen')

import theme from '../../theme'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    firstView: {
        width: '100%',
        height: '47%',
        backgroundColor: theme.PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomStartRadius: 35,
        borderBottomEndRadius: 35,
        marginBottom: 40,
        alignSelf: 'center'
    },
    logo: {
        width: 150,
        height: 150,
        marginBottom: 90
    },
    essentialTxt: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 30,
        position: 'absolute',
        bottom: 50
    },
    input: {
        marginVertical: 10,      
    },
    loginBtn: {
        marginVertical: 20,
    },
    loginTxt: {
        color: theme.PRIMARY_COLOR
    }
});

export default styles;