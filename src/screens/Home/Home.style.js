import { StyleSheet } from 'react-native';

import theme from '../../theme';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    header: {
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: '3%',
    },
    ecbcTxt: {
        color: '#ffffff',
        fontWeight: 'bold'
    },
    idTxt: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 12
    },
    ecbcAndId: {
        marginLeft: 10
    },
    logo: {
        width: 50,
        height: 50,
        position: 'absolute',
        right: 12
    },
    summary: {
        color: '#636e72',
        fontSize: 17,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: '2.9%'
    },
    cardForBabies: {
        width: '30%',
        height: 140,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        borderRadius: 8
    },
    babyCardsView: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        // borderWidth: 1,
        alignItems: 'center',
        paddingVertical: 10
    },
    statusTxt: {
        color: '#636e72',
    },
    numberTxt: {
        fontWeight: 'bold'
    },
    blueView: {
        width: '100%',
        height: 140,
        backgroundColor: theme.PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
    },
    toRegView: {
        flexDirection: 'row',
        width: '75%',
        height: 80,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    toRegTxt: {
        fontWeight: 'bold',
        color: '#636e72',
        fontSize: 17
    },
    onCallTxt: {
        color: '#636e72',
        fontSize: 17,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: '2.9%'
    },
    doctorsView: {
        width: '31%',
        height: 160,
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    imageView: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: theme.PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
    },
    holdingDoctors: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        marginTop: 10,
        // borderWidth: 1
    },
    doctorName: {
        color: '#636e72',
        fontSize: 17,
    },
    onlineTxt: {
        color: theme.PRIMARY_COLOR,
        fontSize: 13
    }
});

export default styles;