import React from 'react'
import { View, Text, ScrollView, Image, StatusBar } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { useSelector } from 'react-redux';

import styles from './Home.style'
import { Header, Card } from '../../components';
import doctors from '../../../assets/data/doctors';
import theme from '../../theme'

const Home = ({ navigation }) => {

    const i18n = useSelector(state => state.LanguageReducer.i18n)

    const summaries = [
        {
            id: 1,
            status: i18n.t('words.admitted'),
            number: 17,
            color: '#0984e3'
        },
        {
            id: 2,
            status: i18n.t('words.discharged'),
            number: 13,
            color: '#b2bec3'
        },
        {
            id: 3,
            status: i18n.t('phrases.high_risk'),
            number: 10,
            color: '#d63031'
        }
    ];

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.PRIMARY_COLOR} />
            <Header style={styles.header}>
                <Ionicons
                    name='menu-outline'
                    color='#ffffff'
                    size={40}
                    onPress={() => navigation.openDrawer()}
                />
                <View style={styles.ecbcAndId}>
                    <Text style={styles.ecbcTxt}>ECEB</Text>
                    <Text style={styles.idTxt}>ID: ***123</Text>
                </View>
                <Image
                    source={require('../../../assets/images/ecbc_logo.png')}
                    style={styles.logo}
                />
            </Header>
            <ScrollView showsVerticalScrollIndicator={false} style={{ width: '100%', paddingHorizontal: 2 }}>
                <Text style={styles.summary}>{i18n.t('phrases.summary_of_24_hours')}</Text>
                <View style={styles.babyCardsView}>
                    {
                        summaries.map(summary => (
                            <Card key={summary.id} style={styles.cardForBabies}>
                                <FontAwesome5 color={summary.color} name='baby' size={40} />
                                <Text style={styles.statusTxt}>{summary.status}</Text>
                                <Text style={[styles.numberTxt, { color: `${summary.color}` }]}>{summary.number}</Text>
                            </Card>
                        ))
                    }
                </View>
                <View style={styles.blueView}>
                    <Card style={styles.toRegView}>
                        <FontAwesome name='edit' color={theme.PRIMARY_COLOR} size={40} />
                        <Text style={styles.toRegTxt}>{i18n.t('phrases.to_register_a_baby')}</Text>
                    </Card>
                </View>
                <Text style={styles.onCallTxt}>{i18n.t('phrases.on_call_doctors')}</Text>
                <View style={styles.holdingDoctors}>
                    {doctors.map(doctor => (
                        <View key={doctor.id} style={styles.doctorsView}>
                            <View style={styles.imageView}>
                                <Image style={{ width: 98, height: 98, borderRadius: 49 }} source={doctor.img} />
                            </View>
                            <Text style={styles.doctorName}>{doctor.name}</Text>
                            <Text style={styles.onlineTxt}>{doctor.online == 1 ? i18n.t('words.online') : i18n.t('words.offline')}</Text>
                        </View>
                    ))}
                </View>
            </ScrollView>
        </View>
    )
}

export default Home
