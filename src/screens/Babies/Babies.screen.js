import React, { useState } from 'react'
import { View, Text, Image, StatusBar, TextInput, SectionList } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { useSelector } from 'react-redux';

import styles from './Babies.style'
import { Header, BabyDetail } from '../../components';
import babies from '../../../assets/data/babies';
import theme from '../../theme'
import COLORS from '../../../assets/data/colors';

const Babies = ({ navigation }) => {

    const [selectedLanguage, setSelectedLanguage] = useState();
    const i18n = useSelector(state => state.LanguageReducer.i18n)

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.PRIMARY_COLOR} />
            <Header style={styles.header}>
                <Ionicons
                    name='menu-outline'
                    color='#ffffff'
                    size={40}
                    onPress={() => navigation.openDrawer()}
                />
                <View style={styles.ecbcAndId}>
                    <Text style={styles.ecbcTxt}>{i18n.t('phrases.list_of_babies')}</Text>
                    <Text style={styles.idTxt}>ID: ***123</Text>
                </View>
                <Image
                    source={require('../../../assets/images/ecbc_logo.png')}
                    style={styles.logo}
                />
            </Header>
            <View style={styles.searchAndSortView}>
                <View style={styles.searchView}>
                    <TextInput
                        style={styles.input}
                        placeholder={i18n.t('phrases.search_the_list_of_babies')}
                        autoCapitalize='none'
                        autoCorrect={false}
                        placeholderTextColor='#ffffff'
                        selectionColor='#ffffff'
                    />
                    <EvilIcons name='search' color='#ffffff' size={25} />
                </View>
                <View style={styles.pickerView}>
                    <Picker
                        style={{ width: '100%', color: '#ffffff' }}
                        dropdownIconColor='#ffffff'
                        itemStyle={{ color: 'red' }}
                        mode='dropdown'
                        selectedValue={selectedLanguage}
                        onValueChange={(itemValue, itemIndex) =>
                            setSelectedLanguage(itemValue)
                        }>
                        <Picker.Item label={i18n.t('words.time')} value="time" />
                        <Picker.Item label={i18n.t('words.status')} value="status" />
                        <Picker.Item label={i18n.t('words.gender')} value="gender" />
                        <Picker.Item label={i18n.t('words.location')} value="location" />
                    </Picker>
                </View>
            </View>
            <SectionList
                sections={babies}
                keyExtractor={(baby) => baby.id}
                renderItem={({ item }) => (
                    <BabyDetail
                        time={item.time}
                        owner={item.owner}
                        gender={item.gender}
                        location={item.location}
                        backgroundColor={COLORS[Math.floor(Math.random() * 4)]}
                    />
                )}
                renderSectionHeader={({ section: { status } }) => (
                    <Text style={styles.sectionHeader}>{status}</Text>
                )}
            />
        </View>
    )
}

export default Babies
