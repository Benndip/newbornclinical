import { StyleSheet } from 'react-native';

import theme from '../../theme';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    header: {
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: '3%',
    },
    ecbcTxt: {
        color: '#ffffff',
        fontWeight: 'bold'
    },
    idTxt: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 12
    },
    ecbcAndId: {
        marginLeft: 10
    },
    logo: {
        width: 50,
        height: 50,
        position: 'absolute',
        right: 12
    },
    searchAndSortView: {
        flexDirection: 'row',
        width: '100%',
        height: 60,
        marginTop: 5,
        alignItems: 'center',
        paddingHorizontal: 5,
        justifyContent: 'space-around'
    },
    searchView: {
        width: '60%',
        height: '87%',
        alignItems: 'center',
        backgroundColor: theme.PRIMARY_COLOR,
        borderRadius: 200,
        paddingTop: 5,
        flexDirection: 'row',
    },
    input: {
        width: '84%',
        height: 40,
        color: '#ffffff',
        paddingHorizontal: 5
    },
    pickerView: {
        width: '37%',
        height: '85%',
        backgroundColor: theme.PRIMARY_COLOR,
        borderRadius: 200,
        justifyContent: 'center',
        paddingHorizontal: 5
    },
    sectionHeader: {
        color: '#222f3e',
        fontSize: 20,
        marginLeft: 10,
        marginVertical: 6,
        fontWeight: 'bold'
    }
});

export default styles;