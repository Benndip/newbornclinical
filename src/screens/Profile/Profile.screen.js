import React, { useState } from 'react';
import { View, Text, StatusBar, ScrollView, Switch, Image } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { useSelector } from 'react-redux';

import styles from './Profile.style';
import { Header, Card } from '../../components';
import babiesForDoctor from '../../../assets/data/babiesForDoctor';
import babyDetails from '../../../assets/data/babyDetails'
import theme from '../../theme'

const Profile = ({ navigation }) => {

    const [selectedItem, setSelectedItem] = useState();
    const [switchValue, setSwitchValue] = useState(false)

    const i18n = useSelector(state => state.LanguageReducer.i18n);

    const babiesForDoctor = [
        {
            id: 1,
            status: i18n.t('words.registered'),
            number: 17,
            color: '#0984e3'
        },
        {
            id: 2,
            status: i18n.t('words.diagnosed'),
            number: 13,
            color: '#d63031'
        },
        {
            id: 3,
            status: i18n.t('words.discharged'),
            number: 10,
            color: '#b2bec3',

        }
    ];

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.PRIMARY_COLOR} />
            <Header style={styles.header}>
                <Ionicons
                    name='menu-outline'
                    color='#ffffff'
                    size={40}
                    onPress={() => navigation.openDrawer()}
                />
                <View style={styles.ecbcAndId}>
                    <Text style={styles.ecbcTxt}>ECEB</Text>
                    <Text style={styles.idTxt}>ID: ***123</Text>
                </View>
                <Image
                    source={require('../../../assets/images/ecbc_logo.png')}
                    style={styles.logo}
                />
            </Header>
            <ScrollView showsVerticalScrollIndicator={false} style={{ width: '100%', paddingHorizontal: 2 }}>
                <Card style={styles.firstCard}>
                    <View style={styles.accountDetailsView}>
                        <Text style={styles.accountDetailTxt}>{i18n.t('phrases.account_details')}</Text>
                        <Text>{i18n.t('words.name')}: Fiona</Text>
                        <Text>ID: ****124</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text>{i18n.t('phrases.share_location')}</Text>
                            <Switch
                                value={switchValue}
                                thumbColor={theme.PRIMARY_COLOR}
                                trackColor={{ false: '#ccc', true: theme.PRIMARY_COLOR }}
                                onValueChange={() => setSwitchValue(prev => !prev)}
                            />
                        </View>
                    </View>
                    <View style={styles.imageView}>
                        <Image style={{ width: 97, height: 97, borderRadius: 48.5 }} source={require('../../../assets/images/doctor3.jpg')} />
                    </View>
                </Card>
                <Card style={styles.secondCard}>
                    <View style={styles.activityTxtAndPickerView}>
                        <Text>{i18n.t('words.activity')}</Text>
                        <View style={styles.pickerView}>
                            <Picker
                                style={{ width: '100%', color: '#ffffff' }}
                                dropdownIconColor='#ffffff'
                                itemStyle={{ color: 'red' }}
                                mode='dropdown'
                                selectedValue={selectedItem}
                                onValueChange={(itemValue, itemIndex) =>
                                    setSelectedItem(itemValue)
                                }>
                                <Picker.Item label={i18n.t('words.time')} value="time" />
                                <Picker.Item label={i18n.t('words.status')} value="status" />
                                <Picker.Item label={i18n.t('words.gender')} value="gender" />
                                <Picker.Item label={i18n.t('words.location')} value="location" />
                            </Picker>
                        </View>
                    </View>
                    <View style={styles.babyCardsView}>
                        {
                            babiesForDoctor.map(babyForDoctor => (
                                <Card key={babyForDoctor.id} style={styles.cardForBabies}>
                                    <FontAwesome5 color={babyForDoctor.color} name='baby' size={40} />
                                    <Text style={styles.statusTxt}>{babyForDoctor.status}</Text>
                                    <Text style={[styles.numberTxt, { color: `${babyForDoctor.color}` }]}>{babyForDoctor.number}</Text>
                                </Card>
                            ))
                        }
                    </View>
                    {
                        babyDetails.map(babyDetail => (
                            <View key={babyDetail.id} style={styles.babyDetailView}>
                                <Text style={{ color: '#576574' }}>{i18n.t('words.baby') + ' ' + babyDetail.babayNumber + ' ' + i18n.t('words.of') + ' ' + babyDetail.owner + i18n.t('phrases.registered_at_the') + ' ' + babyDetail.regAt + ' ' + i18n.t('words.at')}</Text>
                                <Text style={{ color: '#576574', fontSize: 12 }}>{babyDetail.time}</Text>
                                <View style={{ flexDirection: 'row', position: 'absolute', right: 0, bottom: 0 }}>
                                    <Text style={styles.dateAndTimeTxt}>{babyDetail.date}</Text>
                                    <Text style={styles.dateAndTimeTxt}>{babyDetail.time}</Text>
                                </View>
                            </View>
                        ))
                    }
                </Card>
            </ScrollView>
        </View>
    )
}

export default Profile
