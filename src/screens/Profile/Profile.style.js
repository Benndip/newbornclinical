import { StyleSheet } from 'react-native';

import theme from '../../theme';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff'
    },
    header: {
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: '3%',
    },
    ecbcTxt: {
        color: '#ffffff',
        fontWeight: 'bold'
    },
    idTxt: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 12
    },
    ecbcAndId: {
        marginLeft: 10
    },
    logo: {
        width: 50,
        height: 50,
        position: 'absolute',
        right: 12
    },
    firstCard: {
        marginVertical: 15,
        width: '95%',
        height: 200,
        alignSelf: 'center',
        borderRadius: 9,
        flexDirection: 'row',
        padding: 15,
        justifyContent: 'space-between'
    },
    accountDetailsView: {
        justifyContent: 'space-between'
    },
    imageView: {
        width: 100,
        height: 100,
        borderRadius: 50,
        borderWidth: 3,
        borderColor: theme.PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
    },
    accountDetailTxt: {
        color: '#57606f',
        fontWeight: 'bold'
    },
    secondCard: {
        marginVertical: 15,
        width: '95%',
        borderRadius: 9,
        padding: 15,
        alignSelf: 'center',
        flexGrow: 1
    },
    activityTxtAndPickerView: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    pickerView: {
        width: '47%',
        height: '85%',
        backgroundColor: theme.PRIMARY_COLOR,
        borderRadius: 200,
        justifyContent: 'center',
        paddingHorizontal: 5
    },
    cardForBabies: {
        width: '30%',
        height: 140,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        borderRadius: 8
    },
    babyCardsView: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10
    },
    statusTxt: {
        color: '#636e72',
    },
    numberTxt: {
        fontWeight: 'bold'
    },
    babyDetailView: {
        width: '100%',
        borderBottomWidth: 0.3,
        borderColor: '#8395a7',
        marginVertical: 15,
        paddingVertical: 20
    },
    dateAndTimeTxt: {
        color: '#8395a7',
        fontSize: 12,
        marginHorizontal: 5
    }
});

export default styles;