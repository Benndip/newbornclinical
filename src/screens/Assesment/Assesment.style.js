import {StyleSheet} from 'react-native';

import theme from '../../theme';

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        backgroundColor: '#ffffff'
    },
    line: {
        width: '95%',
        alignSelf: 'center',
        borderBottomWidth: 0.3,
        borderColor: '#dddddd'
    }
});

export default styles;