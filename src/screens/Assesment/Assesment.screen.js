import React from 'react'
import { View, StatusBar, FlatList } from 'react-native'

import styles from './Assesment.style';
import { AssesCard } from '../../components'
import assesments from '../../../assets/data/assesments';
import theme from '../../theme';

const Assesment = () => {
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.PRIMARY_COLOR} />
            <FlatList
                style={{ width: '100%' }}
                keyExtractor={item => item.id.toString()}
                data={assesments}
                ItemSeparatorComponent={()=><View style={styles.line} />}
                renderItem={({ item }) => (
                    <AssesCard
                        owner={item.owner}
                        location={item.location}
                        eatTime={item.eatTime}
                        status={item.status}
                    />
                )}
            />
        </View>
    )
}

export default Assesment
