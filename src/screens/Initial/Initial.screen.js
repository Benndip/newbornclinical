import React, { useEffect } from 'react'
import { View, Text, Pressable, TouchableOpacity, ScrollView, StatusBar, Image } from 'react-native'
import { useSelector } from 'react-redux';

import styles from './Initial.style'
import { Card } from '../../components'
import theme from '../../theme'

const Initial = ({ navigation }) => {

    //from redux store
    const i18n = useSelector(state => state.LanguageReducer.i18n)

    useEffect(() => {
        navigation.addListener('beforeRemove', e => {
            e.preventDefault();
        })
    }, [navigation])

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.PRIMARY_COLOR} />
            <View style={styles.firstView}>
                <Image
                    source={require('../../../assets/images/ecbc_logo.png')}
                    style={styles.logo}
                />
                <Text style={styles.essentialTxt}>{i18n.t('phrases.essential_care_for_every_baby')}</Text>
            </View>
            <ScrollView style={{ with: '100%' }}>
                <Card
                    style={styles.card}
                    onPress={() => navigation.navigate('Login')}
                >
                    <Text style={styles.cardTxt}>{i18n.t('words.individual')}</Text>
                </Card>
                <Card
                    style={styles.card}
                    onPress={() => navigation.navigate('Login')}
                >
                    <Text style={styles.cardTxt}>{i18n.t('words.financial')}</Text>
                </Card>
                <View style={styles.viewForTxts}>
                    <Text style={styles.txt}>{i18n.t('phrases.by_continuing_you_agree_to_our')}</Text>
                    <TouchableOpacity>
                        <Text style={styles.touchTxt}>{i18n.t('phrases.privacy_policies')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Text style={styles.touchTxt}>{i18n.t('phrases.data_usage_policies')}</Text>
                    </TouchableOpacity>
                    <Text style={styles.txt}>{i18n.t('phrases.including_our')}</Text>
                    <TouchableOpacity>
                        <Text style={styles.touchTxt}>{i18n.t('phrases.cookies_use')}</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

export default Initial
