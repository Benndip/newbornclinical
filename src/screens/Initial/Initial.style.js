import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('screen')

import theme from '../../theme'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
    },
    firstView: {
        width: '100%',
        height: '47%',
        backgroundColor: theme.PRIMARY_COLOR,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomStartRadius: 35,
        borderBottomEndRadius: 35,
        marginBottom: 40,
        alignSelf: 'center'
    },
    essentialTxt: {
        color: '#ffffff',
        fontWeight: 'bold',
        fontSize: 18,
        position: 'absolute',
        bottom: 50
    },
    logo: {
        width: 150,
        height: 150,
        marginBottom: 90
    },
    card: {
        width: '75%',
        height: 80,
        marginVertical: 15,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    cardTxt: {
        color: theme.PRIMARY_COLOR,
        fontSize: 16,
        fontWeight: 'bold'
    },
    viewForTxts: {
        width: 250,
        flexDirection: 'row',
        flexWrap: 'wrap',
        height: 40,
        alignItems: 'center',
        marginTop: 70,
        alignSelf: 'center'
    },
    txt: {
        fontSize: 12
    },
    touchTxt: {
        color: theme.PRIMARY_COLOR,
        fontSize: 12,
        marginLeft: 2
    }
});

export default styles;