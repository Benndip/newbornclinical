import React from 'react'
import { View, Text } from 'react-native'

import styles from './Alerts.style'

const Alerts = () => {
    return (
        <View style={styles.container}>
            <Text>Alert screen</Text>
        </View>
    )
}

export default Alerts
