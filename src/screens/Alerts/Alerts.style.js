import {StyleSheet} from 'react-native';

import theme from '../../theme';

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff'
    }
});

export default styles;