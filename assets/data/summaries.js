import { useSelector } from 'react-redux';

const summaries = () => {

    const i18n = useSelector(state => state.LanguageReducer.i18n)

    const data = [
        {
            id: 1,
            status: i18n.t('words.admitted'),
            number: 17,
            color: '#0984e3'
        },
        {
            id: 2,
            status: i18n.t('words.discharged'),
            number: 13,
            color: '#b2bec3'
        },
        {
            id: 3,
            status: i18n.t('phrases.high_risk'),
            number: 10,
            color: '#d63031'
        }
    ];

    return data
}

export { summaries };