const assesments = [
    {
        id: 1,
        owner: 'Oni',
        eatTime: '11:06 AM',
        status: 'Normal',
        location: 'Parental Ward',
    },
    {
        id: 2,
        owner: 'Ada',
        eatTime: '11:06 AM',
        status: 'Danger',
        location: 'Parental Ward',
    },
    {
        id: 3,
        owner: 'Teka',
        eatTime: '11:00 AM',
        status: 'Normal',
        location: 'Parental Ward',
    },
    {
        id: 4,
        owner: 'Evi',
        eatTime: '11:01 AM',
        status: 'Danger',
        location: 'Parental Ward',

    },
    {
        id: 5,
        owner: 'Ada',
        eatTime: '11:06 AM',
        status: 'Normal',
        location: 'Parental Ward',
    },
    {
        id: 6,
        owner: 'Oni',
        eatTime: '11:06 AM',
        status: 'Danger',
        location: 'Parental Ward',
    },
    {
        id: 7,
        owner: 'Teka',
        eatTime: '11:00 AM',
        status: 'Normal',
        location: 'Parental Ward',
    },
    {
        id: 8,
        owner: 'Evi',
        eatTime: '11:01 AM',
        status: 'Danger',
        location: 'Parental Ward',
    },
    {
        id: 9,
        owner: 'Oni',
        eatTime: '11:06 AM',
        status: 'Normal',
        location: 'Parental Ward',
    },
    {
        id: 10,
        owner: 'Ada',
        eatTime: '11:06 AM',
        status: 'Danger',
        location: 'Parental Ward',
    },
    {
        id: 11,
        owner: 'Teka',
        eatTime: '11:00 AM',
        status: 'Normal',
        location: 'Parental Ward',
    },
    {
        id: 12,
        owner: 'Evi',
        eatTime: '11:01 AM',
        status: 'Danger',
        location: 'Parental Ward',

    },
];

export default assesments