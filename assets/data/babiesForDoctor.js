const babiesForDoctor = [
    {
        id: 1,
        status: 'Registered',
        number: 17,
        color: '#0984e3'
    },
    {
        id: 2,
        status: 'Diagnosed',
        number: 13,
        color: '#d63031'
    },
    {
        id: 3,
        status: 'Discharged',
        number: 10,
        color: '#b2bec3',

    }
];

export default babiesForDoctor