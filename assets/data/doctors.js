const doctors = [
    {
        id: 1,
        img: require('../images/doctor1.jpg'),
        name: 'Sindie',
        online: 1
    },
    {
        id: 2,
        img: require('../images/doctor2.jpg'),
        name: 'Austin',
        online: 0
    },
    {
        id: 3,
        img: require('../images/doctor3.jpg'),
        name: 'Jeffrey',
        online: 1
    },
];

export default doctors