const babies = [
    {
        id: 1,
        status: 'Recently Added',
        data: [
            {
                id: 1,
                gender: 'male',
                owner: 'Oni',
                location: 'Parental Ward',
                time: 15
            },
            {
                id: 2,
                gender: 'female',
                owner: 'Ada',
                location: 'Parental Ward',
                time: 25
            }
        ]
    },
    {
        id: 2,
        status: 'Past Registered',
        data: [
            {
                id: 1,
                gender: 'male',
                owner: 'Oni',
                location: 'Parental Ward',
                time: 11
            },
            {
                id: 2,
                gender: 'female',
                owner: 'Evi',
                location: 'Parental Ward',
                time: 36
            },
            {
                id: 3,
                gender: 'male',
                owner: 'Oni',
                location: 'Parental Ward',
                time: 10,
            },
            {
                id: 4,
                gender: 'female',
                owner: 'Clerence',
                location: 'Parental Ward',
                time: 45
            }
        ]
    }
];

export default babies;
