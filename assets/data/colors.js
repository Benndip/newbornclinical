const COLORS = [
    {
        dark: '#0984e3',
        light: 'rgba(9, 132, 227,0.3)'
    },
    {
        dark: '#10ac84',
        light: 'rgba(16, 172, 132,0.3)'
    },
    {
        dark: '#fdcb6e',
        light: 'rgba(253, 203, 110,0.3)'
    },
    {
        dark: '#e84393',
        light: 'rgba(232, 67, 147,0.3)'
    },
];

export default COLORS